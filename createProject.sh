#!/bin/bash
#################################################################################
#                             createProject.sh                                  #
#                                                                               #
#  El objetivo de este archivo es crear y transferir las librerias necesarias   #
#  para poder realizar la compilación de toolkit de Arduino.                    #
#                                                                               #
#                                                                               #
#  This program is free software: you can redistribute it and/or modify         #
#  it under the terms of the GNU Lesser General Public License as published by  # 
#  the Free Software Foundation, either version 3 of the License, or            # 
#  (at your option) any later version.                                          #
#  This program is distributed in the hope that it will be useful,              #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of               #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
#  GNU Lesser General Public License for more details.                          #
#                                                                               #
#  You should have received a copy of the GNU General Public License            #
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.       #
#                                                                               #
#                                                                               # 
#################################################################################
CWD=$(pwd)
ARDUINO_SDK="$CWD/ArduinoCore-avr"
ARDUINO_PATH_CORE="$ARDUINO_SDK/cores/arduino"
ARDUINO_PATH_VARIANTS="$ARDUINO_SDK/variants"
rm -Rf $ARDUINO_SDK

if [ ! $# -gt 0 ]; then
	echo "USAGE: createProject.sh -m=<model> || --model=<model>"
	echo " -m=<model>, --model=<model>    <model> must one of the following: uno,nano,mega"
	exit 1
fi

for i in "$@"
do
case $i in
    -m=*|--model=*)
    MODEL="${i#*=}"
    shift # past argument with no value
    ;;
    *)
        echo "USAGE: createProject.sh -m=<model> || --model=<model>"
	echo " -m=<model>, --model=<model>    <model> must one of the following: uno,nano,mega"
	exit 1
    ;;
esac
done


case $MODEL in
	"uno")
		VARIANT="$ARDUINO_PATH_VARIANTS/standard"
		MCU="atmega328p"
		ARDUINO="ARDUINO_AVR_UNO"
		export MCU
		export ARDUINO
	 shift
	 ;;
 	"nano")
		VARIANT="$ARDUINO_PATH_VARIANTS/eightanaloginputs"
		MCU="atmega328p"
		ARDUINO="ARDUINO_AVR_NANO"
		export MCU
		export ARDUINO
		
	 shift
	 ;;
 	"mega")
		VARIANT="$ARDUINO_PATH_VARIANTS/mega"
		MCU="atmega2560"
		ARDUINO="ARDUINO_AVR_MEGA2560"
		export MCU
		export ARDUINO
	 shift
	 ;;
	*)
       	echo "USAGE: createProject.sh -m=<model> || --model=<model>"
        echo " -m=<model>, --model=<model>    <model> must one of the following: uno,nano,mega"
        exit 1
	;;
esac

git clone https://github.com/arduino/ArduinoCore-avr.git

if [ $? -gt 0 ]; then
	echo "Can't get Arduino sources from Arduino GIT repository. Please check yout internet connection."
	exit 1
fi

if ! [ -d $ARDUINO_SDK ]; then
	echo "Can't get Arduino sources from Arduino GIT repository. Please check yout internet connection."
	exit 1
fi

if [[ ! -d $ARDUINO_PATH_CORE   ||  ! -d $ARDUINO_PATH_VARIANTS ]]; then
	echo "Can't get Arduino sources from Arduino GIT repository. Please check yout internet connection."
	exit 1
fi

files=$(shopt -s nullglob dotglob; echo $ARDUINO_PATH_CORE/*)

if [ ! ${#files} -gt 0 ]; then
  echo "Can't get Arduino sources from Arduino GIT repository. Please check yout internet connection."
  exit 1
fi

if [ ! -f "$VARIANT/pins_arduino.h" ]; then
	echo "For Arduino API your Atmel toolchain must contain the Arduino sources."
	exit 1
fi


header_files=$(shopt -s nullglob dotglob; echo $ARDUINO_PATH_CORE/*.h)
header_files="$header_files $VARIANT/pins_arduino.h"
c_files=$(shopt -s nullglob dotglob; echo $ARDUINO_PATH_CORE/*.c)
cpp_files=$(shopt -s nullglob dotglob; echo $ARDUINO_PATH_CORE/*.cpp)
asm_files=$(shopt -s nullglob dotglob; echo $ARDUINO_PATH_CORE/*.S)
source_files="$c_files $cpp_files $asm_files" 


if [[ -d $PWD/libsrc || -d $PWD/headers ]]; then
	echo "The script cannot continue. Already exist src and/or headers directory it could destroy other sources files. Please use an empty directory"
	exit 1
fi
	
echo "Creating project directories..."
mkdir headers libsrc src
echo "Copying header files..."

for file in $header_files
do
	echo "Copy $file to headers directory..."
	cp $file "$PWD/headers"
done

echo "Copying source files..."
for file in $source_files
do
	echo "Copy $file to source directory..."
	filename=$(basename $file)
	if [ "$filename" == "main.cpp" ]; then
		cp $file "$PWD/src"
	else
		cp $file "$PWD/libsrc"
	fi

done

echo "Create Makefile for Arduino model..."
cp Makefile.in Makefile
( shopt -s globstar dotglob
 	 sed -i -- "s/{mcu}/$MCU/g" Makefile
	 sed -i -- "s/{arduino_type}/$ARDUINO/g" Makefile

)

rm -Rf $ARDUINO_SDK
